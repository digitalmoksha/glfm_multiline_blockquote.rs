//! Multiline Blockquote
//!
//! ```
//! >>>
//! If you paste a message from somewhere else
//!
//! that spans multiple lines,
//!
//! you can quote that without having to manually prepend `>` to every line!
//! >>>
//! ```
//!
//! <https://docs.gitlab.com/ee/user/markdown.html#multiline-blockquote>
use markdown_it::{MarkdownIt, Node, NodeValue, Renderer};
use markdown_it::parser::block::{BlockRule, BlockState};
use markdown_it::parser::inline::InlineRoot;
use markdown_it::plugins::cmark::block::blockquote::BlockquoteScanner;

pub fn add(md: &mut MarkdownIt) {
    md.block.add_rule::<MultilineBlockquoteScanner>()
        .before::<BlockquoteScanner>();
}

#[derive(Debug)]
pub struct MultilineBlockquote {
    pub marker: char,
    pub marker_len: usize,
    // pub content: String,
}

impl NodeValue for MultilineBlockquote {
    fn render(&self, node: &Node, fmt: &mut dyn Renderer) {
        fmt.cr();
        fmt.open("blockquote", &node.attrs);
        fmt.cr();
        fmt.contents(&node.children);
        fmt.cr();
        fmt.close("blockquote");
        fmt.cr();
    }
}

#[doc(hidden)]
pub struct MultilineBlockquoteScanner;

impl MultilineBlockquoteScanner {
    fn get_header<'a>(state: &'a mut BlockState) -> Option<(char, usize)> {
        // if it's indented more than 3 spaces, it should be a code block
        if state.line_indent(state.line) >= 4 { return None; }

        let line = state.get_line(state.line);
        let mut chars = line.chars();

        let marker = chars.next()?;
        if marker != '>' { return None; }

        // scan marker length
        let mut len = 1;
        while Some(marker) == chars.next() { len += 1; }

        if len < 3 { return None; }

        Some((marker, len))
    }
}

impl BlockRule for MultilineBlockquoteScanner {
    fn check(state: &mut BlockState) -> Option<()> {
        Self::get_header(state).map(|_| ())
    }

    fn run(state: &mut BlockState) -> Option<(Node, usize)> {
        let (marker, len) = Self::get_header(state)?;

        let mut next_line = state.line;
        let mut have_end_marker = false;

        // search end of block
        'outer: loop {
            next_line += 1;
            if next_line >= state.line_max {
                // unclosed block should be autoclosed by end of document.
                // also block seems to be autoclosed by end of parent
                break;
            }

            let line = state.get_line(next_line);

            if !line.is_empty() && state.line_indent(next_line) < 0 {
                // non-empty line with negative indent should stop the list:
                // - ```
                //  test
                break;
            }

            let mut chars = line.chars().peekable();

            if Some(marker) != chars.next() { continue; }

            if state.line_indent(next_line) >= 4 {
                // closing fence should be indented less than 4 spaces
                continue;
            }

            // scan marker length
            let mut len_end = 1;
            while Some(&marker) == chars.peek() {
                chars.next();
                len_end += 1;
            }

            // closing code fence must be at least as long as the opening one
            if len_end < len { continue; }

            // make sure tail has spaces only
            loop {
                match chars.next() {
                    Some(' ' | '\t') => {},
                    Some(_) => continue 'outer,
                    None => {
                        have_end_marker = true;
                        break 'outer;
                    }
                }
            }
        }

        // If a fence has heading spaces, they should be removed from its inner block
        let indent = state.line_offsets[state.line].indent_nonspace;
        // let (content, _) = state.get_lines(state.line + 1, next_line, indent as usize, true);
        // let (content, mapping) = state.get_lines(start_line, next_line, state.blk_indent, false);
        let (content, mapping) = state.get_lines(state.line + 1, next_line, state.blk_indent, true);

        let mut node = Node::new(MultilineBlockquote {
            marker,
            marker_len: len,
            // content,
        });
        node.children.push(Node::new(InlineRoot::new(content, mapping)));

        Some((node, next_line - state.line + if have_end_marker { 1 } else { 0 }))
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn it_works() {
        let result = add(2, 2);
        assert_eq!(result, 4);
    }
}
