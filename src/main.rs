use markdown_it::parser::inline::{Text, TextSpecial};
// use std::collections::HashMap;
use std::io::Read;
use std::io::Write;

// use glfm_multiline_blockquote;

fn main() {
    let input = "-".to_owned();
    let output = "-".to_owned();
    let vec = if input == "-" {
        let mut vec = Vec::new();
        std::io::stdin().read_to_end(&mut vec).unwrap();
        vec
    } else {
        std::fs::read(input).unwrap()
    };

    let source = String::from_utf8_lossy(&vec);
    // let options = HashMap;
    let result = render(source.to_string());

    if output == "-" {
        std::io::stdout().write_all(result.as_bytes()).unwrap();
    } else {
        std::fs::write(output, &result).unwrap();
    }
}

// pub fn render(text: String, options: HashMap<K, V>) -> String {
pub fn render(text: String) -> String {
    let md = &mut markdown_it::MarkdownIt::new();

    // let sourcepos: bool = options["sourcepos"];
    // let self_closing_tags: bool = options["self_closing_tags"];
    // let debug: bool = options["debug"];
    let sourcepos: bool = true;
    let self_closing_tags: bool = true;
    let debug: bool = false;

    // if debug { println!("{:?}", options) }

    markdown_it::plugins::cmark::add(md);
    markdown_it::plugins::extra::tables::add(md);
    markdown_it::plugins::extra::strikethrough::add(md);
    markdown_it::plugins::extra::beautify_links::add(md);
    markdown_it::plugins::html::add(md);
    markdown_it::plugins::extra::linkify::add(md);

    glfm_multiline_blockquote::add(md);

    if sourcepos {
        markdown_it::plugins::sourcepos::add(md);
    }

    let ast = md.parse(&text);

    if debug {
        ast.walk(|node, depth| {
            print!("{}", "    ".repeat(depth as usize));
            let name = &node.name()[node.name().rfind("::").map(|x| x+2).unwrap_or_default()..];
            if let Some(data) = node.cast::<Text>() {
                println!("{}: {:?}", name, data.content);
            } else if let Some(data) = node.cast::<TextSpecial>() {
                println!("{}: {:?}", name, data.content);
            } else {
                println!("{}", name);
            }
        });
    }

    if self_closing_tags {
        // render using self-closing tags, such as <br />
        ast.xrender()
    } else {
        ast.render()
    }
}
